function createCard(name, description, pictureUrl, startDate, endDate, locationName) {
    return `
    <div class="col-4">
      <div class="card shadow-lg p-3 mb-5 bg-grey rounded">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${locationName}</h6>
          <p class="card-text">${description}</p>
        </div>
        <div class = "card-footer">
        ${startDate} - ${endDate}
        </div>
      </div>
    </div>
    `;
  }


window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
        throw new Error("Response not ok")
      } else {
        const data = await response.json();

        for (let conference of data.conferences) {
            const detailUrl = `http://localhost:8000${conference.href}`;
            const detailResponse = await fetch(detailUrl);
            if (detailResponse.ok) {
              const details = await detailResponse.json();
              const title = details.conference.name;
              const description = details.conference.description;
              const pictureUrl = details.conference.location.picture_url;
              const startDate = details.conference.starts;
              const start = new Date(startDate).toLocaleDateString();
              const endDate = details.conference.ends;
              const end = new Date(endDate).toLocaleDateString();
              const locationName = details.conference.location.name
              const html = createCard(title, description, pictureUrl, start, end, locationName);
              const column = document.querySelector('.row');
              column.innerHTML += html;
            }
          }

        }
    } catch (e) {
        console.error('error', error)
    }

  });
