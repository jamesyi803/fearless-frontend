// detect when DOM is loaded on the page
// set selectTag equal to id "conference"

window.addEventListener('DOMContentLoaded', async () => {
    const selectTag = document.getElementById('conference');

    // set url equal to localhost
    // send get request to url by using fetch and await and set it to reponse
    // if response is ok
    // turn the response into json and set it to data
    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();

      // for every conference in data.conferences
      // set option equal to element "option"
      // value of option is conference.href - id of conference
      // innerHTML of option is conference.name - what shows up on drop down menu page
      // selectTag appends option - drop down menu shows

      for (let conference of data.conferences) {
        const option = document.createElement('option');
        option.value = conference.href;
        option.innerHTML = conference.name;
        selectTag.appendChild(option);
      }

      // take away d-none from selectTag (now it shows)
      // set loadingTag to id "loading-conference-spinner"
      // add d-none to loadingTag
      // set formTag to id "create-attendee-form"
      // when formTag gets submitted
      // prevent default action
      // set formData to new formData from formTag that just got submitted
      // convert data to json and set it to json
      // set attendeeUrl
      selectTag.classList.remove('d-none')
      const loadingTag = document.getElementById('loading-conference-spinner')
      loadingTag.classList.add('d-none')
      const formTag = document.getElementById('create-attendee-form');
        formTag.addEventListener('submit', async event => {
            event.preventDefault();
            const formData = new FormData(formTag);
            const json = JSON.stringify(Object.fromEntries(formData));
            const attendeeUrl = 'http://localhost:8001/api/attendees/';
            const fetchConfig = {
                method: "post",
                body: json,
                headers: {
                    'Content-Type': 'application/json',
                },
            };
            const response = await fetch(attendeeUrl, fetchConfig);
            if (response.ok) {
                formTag.classList.add('d-none')
                const alertTag = document.getElementById('success-message')
                alertTag.classList.remove('d-none')
                formTag.reset();
                const newAttendee = await response.json();
                console.log(newAttendee)
                }
        })
    }

  });
